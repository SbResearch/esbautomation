package com.esbautomation.component;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

import com.esbautomation.global.GlobalUtil;
import com.esbautomation.model.TestStep;
import com.esbautomation.model.TestStepResult;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class ResultGeneration implements Callable{

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		// TODO Auto-generated method stub
		TestStep testStep = (TestStep) eventContext.getMessage().getInvocationProperty("steps");
		String testKey=(String) eventContext.getMessage().getInvocationProperty("testKeyno");
		String resStatus=(String) eventContext.getMessage().getInvocationProperty("resStatus");
		System.out.println("h12 : "+testStep);
		System.out.println("key is : "+testKey);
		TestStepResult testStepResult=new TestStepResult();
		testStepResult.setTestStepKey(testKey);
		testStepResult.setStepNo(testStep.getTag());
		testStepResult.setStatus(resStatus);
		GlobalUtil.ResultList.add(testStepResult);
		System.out.println("Final: "+GlobalUtil.ResultList);
		Gson gson = new Gson();
		String Json=gson.toJson(GlobalUtil.ResultList);
		JSONArray jaaray=new JSONArray(Json);
			
		return jaaray;
	}

}
