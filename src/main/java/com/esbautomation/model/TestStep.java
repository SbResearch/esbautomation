
package com.esbautomation.model;



public class TestStep{


	private String CalculationDate;
   
    private String TransactionType;
   
    private String Expected;
    
    private String Tag;
    
    private String recordType;
    
    private String email;

	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getExpected() {
		return Expected;
	}

	public void setExpected(String expected) {
		Expected = expected;
	}

	public String getTag() {
		return Tag;
	}

	public void setTag(String tag) {
		Tag = tag;
	}

	public String getCalculationDate() {
		return CalculationDate;
	}

	public void setCalculationDate(String calculationDate) {
		CalculationDate = calculationDate;
	}

	public String getTransactionType() {
		return TransactionType;
	}

	public void setTransactionType(String transactionType) {
		TransactionType = transactionType;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	@Override
	public String toString() {
		return "TestStep [CalculationDate=" + CalculationDate + ", TransactionType=" + TransactionType + ", Expected="
				+ Expected + ", Tag=" + Tag + ", recordType=" + recordType + ", email=" + email + "]";
	}

	

	

	
   


}
