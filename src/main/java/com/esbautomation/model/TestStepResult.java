package com.esbautomation.model;

public class TestStepResult {

	private String testStepKey;
	private String stepNo;
	private String status;
	public String getTestStepKey() {
		return testStepKey;
	}
	public void setTestStepKey(String testStepKey) {
		this.testStepKey = testStepKey;
	}
	public String getStepNo() {
		return stepNo;
	}
	public void setStepNo(String stepNo) {
		this.stepNo = stepNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "TestStepResult [testStepKey=" + testStepKey + ", stepNo=" + stepNo + ", status=" + status + "]";
	}
	
	
	
}
