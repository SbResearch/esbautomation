<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet  
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	version="2.0">
	
	<xsl:output method="xml" indent="yes" />
	<xsl:param name="email"/>
	<xsl:param name="TransactionType"/>

	<xsl:template match="/">
	 <saverequest>
	    <email><xsl:value-of select="$email"></xsl:value-of></email>
	    <TransactionType><xsl:value-of select="$TransactionType"></xsl:value-of></TransactionType> 
	 </saverequest>	
	</xsl:template>	
</xsl:stylesheet>
